package ru.t1.dsinetsky.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.dto.request.system.SystemAboutRequest;
import ru.t1.dsinetsky.tm.dto.request.system.SystemVersionRequest;
import ru.t1.dsinetsky.tm.dto.response.system.SystemAboutResponse;
import ru.t1.dsinetsky.tm.dto.response.system.SystemVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISystemEndpoint extends IEndpoint {

    @NotNull
    String NAME = "SystemEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, ISystemEndpoint.class);
    }

    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, ISystemEndpoint.class);
    }

    @NotNull
    @WebMethod
    SystemAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final SystemAboutRequest request
    );

    @NotNull
    @WebMethod
    SystemVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final SystemVersionRequest request
    );

}
