package ru.t1.dsinetsky.tm.dto.request.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public final class UserViewProfileRequest extends AbstractUserRequest {

    public UserViewProfileRequest(final @Nullable String token) {
        super(token);
    }

}
