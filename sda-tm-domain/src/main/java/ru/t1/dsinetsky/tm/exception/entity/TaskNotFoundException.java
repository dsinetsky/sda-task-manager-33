package ru.t1.dsinetsky.tm.exception.entity;

public final class TaskNotFoundException extends GeneralEntityException {

    public TaskNotFoundException() {
        super("Task not found!");
    }

}
