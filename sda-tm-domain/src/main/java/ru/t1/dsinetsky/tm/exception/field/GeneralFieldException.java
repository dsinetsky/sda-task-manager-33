package ru.t1.dsinetsky.tm.exception.field;

import ru.t1.dsinetsky.tm.exception.GeneralException;

public class GeneralFieldException extends GeneralException {

    public GeneralFieldException() {
    }

    public GeneralFieldException(final String message) {
        super(message);
    }

    public GeneralFieldException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public GeneralFieldException(final Throwable cause) {
        super(cause);
    }

    public GeneralFieldException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
