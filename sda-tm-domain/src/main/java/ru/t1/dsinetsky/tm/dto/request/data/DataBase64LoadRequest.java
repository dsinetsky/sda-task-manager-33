package ru.t1.dsinetsky.tm.dto.request.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public final class DataBase64LoadRequest extends AbstractUserRequest {

    public DataBase64LoadRequest(final @Nullable String token) {
        super(token);
    }

}
