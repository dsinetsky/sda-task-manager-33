package ru.t1.dsinetsky.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.command.AbstractCommand;
import ru.t1.dsinetsky.tm.exception.system.InvalidArgumentException;
import ru.t1.dsinetsky.tm.exception.system.InvalidCommandException;

import java.util.Collection;

public interface ICommandService {

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

    @NotNull
    Collection<AbstractCommand> getProjectCommands();

    @NotNull
    Collection<AbstractCommand> getTaskCommands();

    @NotNull
    Collection<AbstractCommand> getDataCommands();

    @NotNull
    Collection<AbstractCommand> getUserCommands();

    @NotNull
    Collection<AbstractCommand> getSystemCommands();

    @NotNull
    Collection<AbstractCommand> getArgumentCommands();

    void add(@NotNull AbstractCommand command);

    @NotNull
    AbstractCommand getCommandByName(@Nullable String commandName) throws InvalidCommandException;

    @NotNull
    AbstractCommand getCommandByArgument(@Nullable String argument) throws InvalidArgumentException;

}
