package ru.t1.dsinetsky.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.api.service.IPropertyService;

import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull
    private final static String PROPERTIES_FILE = "application.properties";

    @NotNull
    private final static String APPLICATION_VERSION_KEY = "version";

    @NotNull
    private final static String SERVER_PORT = "server.port";

    @NotNull
    private final static String SERVER_PORT_DEFAULT = "8088";

    @NotNull
    private final static String SERVER_HOST = "server.host";

    @NotNull
    private final static String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private final static String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private final static String PASSWORD_SECRET_DEFAULT = "456487";

    @NotNull
    private final static String PASSWORD_ITERATION_DEFAULT = "10005";


    @NotNull
    private final static String EMPTY_VALUE = "";

    @NotNull
    private final static Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(PROPERTIES_FILE));
    }

    @NotNull
    private String getEnvFormat(@NotNull final String value) {
        return value.replace(".", "_").toUpperCase();
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String keyEnvFormat = getEnvFormat(key);
        if (System.getenv().containsKey(keyEnvFormat)) return System.getenv(keyEnvFormat);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @Override
    public int getPasswordIteration() {
        return Integer.parseInt(getStringValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT));
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    public String getPort() {
        return getStringValue(SERVER_PORT, SERVER_PORT_DEFAULT);
    }

    @NotNull
    public String getHost() {
        return getStringValue(SERVER_HOST);
    }

    @Override
    @NotNull
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

}
