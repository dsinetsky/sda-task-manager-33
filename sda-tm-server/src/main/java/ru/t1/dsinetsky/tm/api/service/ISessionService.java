package ru.t1.dsinetsky.tm.api.service;

import ru.t1.dsinetsky.tm.model.Session;

public interface ISessionService extends IAbstractUserOwnedService<Session> {
}
