package ru.t1.dsinetsky.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.dto.Domain;

public interface IDomainService {

    @NotNull
    @SneakyThrows
    Domain saveDataBackup();

    @NotNull
    @SneakyThrows
    Domain loadDataBackup();

    @NotNull
    @SneakyThrows
    Domain saveDataBase64();

    @NotNull
    @SneakyThrows
    Domain loadDataBase64();

    @NotNull
    @SneakyThrows
    Domain saveDataBinary();

    @NotNull
    @SneakyThrows
    Domain loadDataBinary();

    @NotNull
    @SneakyThrows
    Domain saveDataToJsonWithFasterXml();

    @NotNull
    @SneakyThrows
    Domain loadDataFromJsonWithFasterXml();

    @NotNull
    @SneakyThrows
    Domain saveDataToJsonWithJaxB();

    @NotNull
    @SneakyThrows
    Domain loadDataFromJsonWithJaxB();

    @NotNull
    @SneakyThrows
    Domain saveDataToXmlWithFasterXml();

    @NotNull
    @SneakyThrows
    Domain loadDataFromXmlWithFasterXml();

    @NotNull
    @SneakyThrows
    Domain saveDataToXmlWithJaxB();

    @NotNull
    @SneakyThrows
    Domain loadDataFromXmlWithJaxB();

    @NotNull
    @SneakyThrows
    Domain saveDataToYamlWithFasterXml();

    @NotNull
    @SneakyThrows
    Domain loadDataFromYamlWithFasterXml();

}
