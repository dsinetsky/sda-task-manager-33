package ru.t1.dsinetsky.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.endpoint.IAuthEndpoint;
import ru.t1.dsinetsky.tm.api.service.IServiceLocator;
import ru.t1.dsinetsky.tm.dto.request.user.UserLoginRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserLogoutRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.dsinetsky.tm.dto.response.user.UserLoginResponse;
import ru.t1.dsinetsky.tm.dto.response.user.UserLogoutResponse;
import ru.t1.dsinetsky.tm.dto.response.user.UserViewProfileResponse;
import ru.t1.dsinetsky.tm.model.Session;
import ru.t1.dsinetsky.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.dsinetsky.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(final @NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @NotNull final String token = serviceLocator.getAuthService().login(login, password);
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) {
        serviceLocator.getAuthService().logout(request.getToken());
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserViewProfileResponse viewProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserViewProfileRequest request
    ) {
        @NotNull final Session session = serviceLocator.getAuthService().validateToken(request.getToken());
        @Nullable final User user = serviceLocator.getUserService().findById(session.getUserId());
        return new UserViewProfileResponse(user);
    }

}
