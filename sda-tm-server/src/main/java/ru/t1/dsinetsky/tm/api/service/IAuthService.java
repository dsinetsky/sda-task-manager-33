package ru.t1.dsinetsky.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.user.AccessDeniedException;
import ru.t1.dsinetsky.tm.exception.user.GeneralUserException;
import ru.t1.dsinetsky.tm.model.Session;
import ru.t1.dsinetsky.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(@NotNull String login, @NotNull String password) throws GeneralException;

    @NotNull
    String login(@Nullable String login, @Nullable String password) throws GeneralException;

    void logout(@Nullable String token) throws GeneralException;

    @NotNull
    Session validateToken(@Nullable String token) throws AccessDeniedException;

    User check(@Nullable String login, @Nullable String password) throws GeneralUserException;

    void lockUserByLogin(@Nullable String login) throws GeneralException;

    void unlockUserByLogin(@Nullable String login) throws GeneralException;
}
