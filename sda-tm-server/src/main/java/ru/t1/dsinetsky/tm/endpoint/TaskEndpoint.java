package ru.t1.dsinetsky.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.endpoint.ITaskEndpoint;
import ru.t1.dsinetsky.tm.api.service.IServiceLocator;
import ru.t1.dsinetsky.tm.api.service.ITaskService;
import ru.t1.dsinetsky.tm.dto.request.task.*;
import ru.t1.dsinetsky.tm.dto.response.task.*;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.model.Session;
import ru.t1.dsinetsky.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.t1.dsinetsky.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    private final ITaskService taskService = serviceLocator.getTaskService();

    public TaskEndpoint(final @NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCreateRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = session.getUserId();
        @NotNull final Task task = taskService.create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskFindByIdResponse findTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskFindByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = session.getUserId();
        @NotNull final Task task = taskService.findById(userId, id);
        return new TaskFindByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskFindByIndexResponse findTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskFindByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        final int index = request.getIndex();
        @Nullable final String userId = session.getUserId();
        @NotNull final Task task = taskService.findByIndex(userId, index);
        return new TaskFindByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskListByProjectResponse listTaskByProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskListByProjectRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String userId = session.getUserId();
        @NotNull final List<Task> tasks = taskService.returnTasksOfProject(userId, projectId);
        return new TaskListByProjectResponse(tasks);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskListResponse listTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskListRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @NotNull final List<Task> tasks = taskService.returnAll(userId);
        return new TaskListResponse(tasks);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = session.getUserId();
        @NotNull final Task task = taskService.removeById(userId, id);
        return new TaskRemoveByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskRemoveByIndexResponse removeTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        final int index = request.getIndex();
        @Nullable final String userId = session.getUserId();
        @NotNull final Task task = taskService.removeByIndex(userId, index);
        return new TaskRemoveByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskStartByIdResponse startTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String id = request.getId();
        @NotNull final Status status = Status.IN_PROGRESS;
        @Nullable final String userId = session.getUserId();
        @NotNull final Task task = taskService.changeStatusById(userId, id, status);
        return new TaskStartByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskStartByIndexResponse startTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        final int index = request.getIndex();
        @NotNull final Status status = Status.IN_PROGRESS;
        @Nullable final String userId = session.getUserId();
        @NotNull final Task task = taskService.changeStatusByIndex(userId, index, status);
        return new TaskStartByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskUnbindFromProjectResponse unbindTaskFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUnbindFromProjectRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String userId = session.getUserId();
        serviceLocator.getProjectTaskService().unbindProjectById(userId, projectId, taskId);
        return new TaskUnbindFromProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = session.getUserId();
        @NotNull final Task task = taskService.updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskUpdateByIndexResponse updateTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        final int index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final String userId = session.getUserId();
        @NotNull final Task task = taskService.updateByIndex(userId, index, name, description);
        return new TaskUpdateByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskBindToProjectRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String userId = session.getUserId();
        serviceLocator.getProjectTaskService().bindProjectById(userId, projectId, taskId);
        return new TaskBindToProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable final String userId = session.getUserId();
        @NotNull final Task task = taskService.changeStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        final int index = request.getIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable final String userId = session.getUserId();
        @NotNull final Task task = taskService.changeStatusByIndex(userId, index, status);
        return new TaskChangeStatusByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskClearRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        taskService.clear(userId);
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskCompleteByIdResponse completeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIdRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String id = request.getId();
        @Nullable final Status status = Status.COMPLETED;
        @Nullable final String userId = session.getUserId();
        @NotNull final Task task = taskService.changeStatusById(userId, id, status);
        return new TaskCompleteByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskCompleteByIndexResponse completeTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIndexRequest request
    ) {
        @NotNull final Session session = check(request);
        final int index = request.getIndex();
        @Nullable final Status status = Status.COMPLETED;
        @Nullable final String userId = session.getUserId();
        @NotNull final Task task = taskService.changeStatusByIndex(userId, index, status);
        return new TaskCompleteByIndexResponse(task);
    }

}
